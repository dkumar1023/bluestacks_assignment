import tkinter
import tkinter as tk
from datetime import date
from tkinter import *
from tkinter import messagebox, filedialog
from tkinter.ttk import Progressbar
from os import path
import requests


def Widgets():
    global bar
    head_label = Label(root, text="File Downloader",
                       padx=15,
                       pady=15,
                       font="SegoeUI 14",
                       bg="palegreen1",
                       fg="red")
    head_label.grid(row=1,
                    column=1,
                    pady=10,
                    padx=5,
                    columnspan=3)

    link_label = Label(root,
                       text="Paste your download URL here:",
                       bg="salmon",
                       pady=5,
                       padx=5)
    link_label.grid(row=2,
                    column=0,
                    pady=5,
                    padx=5)

    root.linkText = Entry(root,
                           width=35,
                          textvariable=file_Link,
                          font="Arial 14")
    root.linkText.grid(row=2,
                       column=1,
                       pady=5,
                       padx=5,
                       columnspan=2)

    destination_label = Label(root,
                              text="Download this file to:",
                              bg="salmon",

                              pady=5,
                              padx=9)
    destination_label.grid(row=3,
                           column=0,
                           pady=5,
                           padx=5)

    root.destinationText = Entry(root,
                                 width=27,
                                 textvariable=download_folder,
                                 font="Arial 14")
    root.destinationText.grid(row=3,
                              column=1,
                              pady=5,
                              padx=5)

    browse_B = Button(root,
                      text="Change",
                      command=Browse,
                      width=10,
                      bg="bisque",
                      relief=GROOVE)
    browse_B.grid(row=3,
                  column=2,
                  pady=1,
                  padx=1)

    bar.grid(row = 5,pady=20,padx=20,column=1)
    #bar.pack(pady=20)

    # Create a button
    b = Button(root, text="Download", command=Download)#.pack(pady=20)
    b.grid(pady=20,row=4,padx=20,column=1)


def Browse():

    download_directory = filedialog.askdirectory(
        initialdir="YOUR DIRECTORY PATH", title="Save File")
    print(download_directory)
    download_folder.set(download_directory)


def Download():
    if file_Link.get() == '':
        tkinter.messagebox.showerror(title='Invalid URL', message='Enter a valid URL')
        return
    if download_folder.get() =='':
        tkinter.messagebox.showerror(title='No Destination Path', message='Enter valid destination path')
        return
    if not path.exists(download_folder.get()):
        tkinter.messagebox.showerror(title='Invalid Destination', message='Enter valid destination path')
        return
    b.focus_displayof()
    with requests.get(file_Link.get()) as r:

        fname = ''
        if "Content-Disposition" in r.headers.keys():
            fname = re.findall("filename=(.+)", r.headers["Content-Disposition"])[0]
        else:
            fname = file_Link.get().split("/")[-1]

        print(fname)

    
    with open(download_folder.get()+'/'+fname, 'wb') as f:
        try:
            url = file_Link.get()
            response = requests.get(url, stream=True)
            print(response.status_code)
            if response.status_code != 200:
                tkinter.messagebox.showerror(title='Invalid URL', message='Kindly check the download URL')
                return
        except:
            tkinter.messagebox.showerror(title='Invalid URL', message='Kindly check the download URL')
            return

        total = response.headers.get('content-length')

        if total is None:
            f.write(response.content)
        else:
            downloaded = 0
            total = int(total)
            for data in response.iter_content(chunk_size=max(int(total / 1000), 1024 * 1024)):
                downloaded += len(data)
                f.write(data)
                done = int(50 * downloaded / total)
                bar['value']=2*done
                root.update_idletasks()
                sys.stdout.write('\r[{}{}]'.format('█' * done, '.' * (50 - done)))
                sys.stdout.flush()
    sys.stdout.write('\n')




root = tk.Tk()
root.geometry("720x400")
root.resizable(False, False)
root.title("File Downloader")
root.config(background="Grey")
file_Link = StringVar()
download_folder = StringVar()

head_label = Label(root, text="File Downloader",padx=10,pady=10,font="SegoeUI 14",bg='bisque',fg="black")
head_label.grid(row=1,
                    column=1,
                    pady=5,
                    padx=5,
                    columnspan=2)

link_label = Label(root,
                       text="Paste your download URL here:",
                       bg="bisque",
                       pady=5,
                       padx=5)
link_label.grid(row=2,
                    column=0,
                    pady=5,
                    padx=5)

root.linkText = Entry(root,
                           width=35,
                          textvariable=file_Link,
                          font="Arial 14")
root.linkText.grid(row=2,
                       column=1,
                       pady=5,
                       padx=5,
                       columnspan=2)

destination_label = Label(root,
                             text="Download this file to:",
                              bg="bisque",
                              pady=5,
                              padx=9)
destination_label.grid(row=3,
                           column=0,
                           pady=5,
                           padx=5)

root.destinationText = Entry(root,
                                 width=27,
                                 textvariable=download_folder,
                                 font="Arial 14")
root.destinationText.grid(row=3,
                              column=1,
                              pady=5,
                              padx=5)

browse_B = Button(root,
                      text="Change",
                      command=Browse,
                      width=10,
                      bg="bisque",
                      relief=GROOVE)
browse_B.grid(row=3,
                  column=2,
                  pady=1,
                  padx=1)

progress_label_text = Label(root,bg='bisque',text="Download progress")
progress_label_text.grid(row=5,pady=10,padx=10,column=0)
bar= Progressbar(root, orient=HORIZONTAL, length=300)
bar.grid(row = 5,pady=20,padx=20,column=1)

b = Button(root, text="Download",bg="bisque" ,command=Download)#.pack(pady=20)
b.grid(pady=20,row=4,padx=20,column=1)


root.mainloop()