import sys
import time
import pyautogui
import os
from selenium import webdriver
from os import path


driver = webdriver.Chrome('chromedriver.exe')
driver.implicitly_wait(30)
driver.maximize_window()
driver.get('https://www.bluestacks.com/')

time.sleep(10)

driver.find_element_by_xpath('/html/body/div[1]/section[1]/div[2]/div/div/div[1]/a[2]').click()

folder_path = '' #Download path ,For eg:- C:\Users\administrator\Downloads\
exe_path=None

def check_exe_presence(folder_path):
    global exe_path
    files = os.listdir(folder_path.strip())

    for f in files:
        if 'BlueStacks' in f:
            exe_path = folder_path.strip()+f
            return True
        else:
            continue
    return False


count = 0
while not check_exe_presence(folder_path):
    count += 1
    time.sleep(60)
    if count == 10:
        sys.exit(1)


os.startfile(exe_path)
time.sleep(10)
print('hi')
x, y = pyautogui.locateCenterOnScreen('Install.PNG')
print(x,y)
pyautogui.moveTo(x, y,8)
time.sleep(2)
start = time.time()
pyautogui.click(x,y)

while pyautogui.locateCenterOnScreen('Play.PNG') is None:
    time.sleep(20)
    continue

done = time.time()
time_taken = done-start

print(time_taken)
